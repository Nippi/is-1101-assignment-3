#include <stdio.h>
#include <stdlib.h>

int main()
{ int number;
    printf("Enter Any Number\n");
    scanf("%d",&number);
    if(number < 0){
        printf("It is a Negative Number\n",number);
    }
    else if(number == 0){
        printf("It is Zero\n",number);
    }
    else if(number > 0){
        printf("It is a Positive Number\n",number);
    }
    else{
        printf("Invalid Number\n");
    }
    return 0;
}
